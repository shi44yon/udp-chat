﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

public class SimpleUdpSrvr
{
    public static void Main()
    {
        int recv;
        string input;
        byte[] data = new byte[1024];
        
        IPEndPoint ipep = new IPEndPoint(IPAddress.Any, 9050);
        
        Socket newsock = new Socket(AddressFamily.InterNetwork,
                        SocketType.Dgram, ProtocolType.Udp);
        
        Console.Write("Enter userID: ");
        string serverName = Console.ReadLine();
        
        newsock.Bind(ipep);
        Console.WriteLine("Waiting for a client...");
        
        IPEndPoint sender = new IPEndPoint(IPAddress.Any, 0);
        EndPoint Remote = (EndPoint)(sender);
        
        recv = newsock.ReceiveFrom(data, ref Remote);
        
        Console.Write("Message received from {0}: ", Remote.ToString());
        Console.WriteLine(Encoding.ASCII.GetString(data, 0, recv));
        
        data = Encoding.ASCII.GetBytes(serverName);
        newsock.SendTo(data, data.Length, SocketFlags.None, Remote);
        
        recv = newsock.ReceiveFrom(data, ref Remote);
        string clientName = Encoding.ASCII.GetString(data, 0, recv);
        
        while(true)
        {
            data = new byte[1024];
            recv = newsock.ReceiveFrom(data, ref Remote);
            
            Console.Write(serverName + ": ");
            Console.WriteLine(Encoding.ASCII.GetString(data, 0, recv));
            newsock.SendTo(data, recv, SocketFlags.None, Remote);
            
            input = Console.ReadLine();
            newsock.SendTo(data, recv, SocketFlags.None, Remote);
        }
    }
}